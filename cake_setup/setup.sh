#!/bin/sh
appPath="app"

# If we have composer. We install everything from scratch
if [ -f "composer.json" ]; then
	if [ -d "$appPath/Vendor" ]; then
		rm -rf $appPath/Vendor
	fi
	if [ -f composer.phar ]
		php composer.phar selfupdate
	else;
		curl -sS https://getcomposer.org/installer | php
	fi
	php composer.phar install

	oldStr="//define('CAKE_CORE_INCLUDE_PATH', ROOT . DS . 'lib')"
	newStr="define('CAKE_CORE_INCLUDE_PATH', ROOT . DS . APP_DIR . DS . Vendor . DS . 'cakephp' . DS . 'cakephp' . DS . 'lib')"

	sed -i "s/$oldStr/$newStr/g" $appPath/webroot/index.php
	sed -i "s/$oldStr/$newStr/g" $appPath/webroot/test.php
fi

# Same for bower
if [ -f "bower.json" ]; then
	if [ -d "$appPath/webroot/components" ] then;
		rm -rf $appPath/webroot/components
	fi
	if [ -d "$appPath/webroot/bower_components" ] then;
		rm -rf $appPath/webroot/bower_components
	fi
fi

# If there are some git submodules, load them
if [ -f ".gitmodules" ]; then;
	git submodute update --init
fi

if [! -f ".htaccess" ]; then;
	touch .htaccess
	echo "<IfModule mod_rewrite.c>\n" >> .htaccess
	echo "\tRewriteEngine on\n" >> .htaccess
	echo "\tRewriteRule    ^$    $appPath/webroot/    [L]\n" >> .htaccess
	echo "\tRewriteRule    (.*) $appPath/webroot/$1    [L]\n" >> .htaccess
	echo "</IfModule>" >> .htaccess
fi

